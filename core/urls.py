from drf_yasg import openapi
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view as swagger_get_schema_view

#   Variables Definitions
schema_view = swagger_get_schema_view(
    openapi.Info(
        title = 'AMSOFT API',
        default_version = 'V1',
        description = "API documentation of application that wil return informations about a management of a association",
        terms_of_service = 'http://localhost:8080/#',
        license = openapi.License(name = 'Apache 2.0', url = 'https://www.apache.org/licenses/LICENSE-2.0.html'),
        contact = openapi.Contact(name = 'MAI', email='aymarmbobda1597@gmail.com', url = 'http://localhost:8080/#'),
    ),
    public = True,
    permission_classes = [permissions.AllowAny, ],
)

urlpatterns = [
    path('admin/', admin.site.urls),    path('redoc/', schema_view.with_ui('redoc', cache_timeout = 0), name = 'schema-redoc'),
    path('docs/', schema_view.with_ui('swagger', cache_timeout = 0), name = 'swagger-schema'),
    path('api/', 
        include([
            path('api.json/', schema_view.without_ui(cache_timeout = 0), name = 'schema-swagger-ui'),
        ])
    ),
]
